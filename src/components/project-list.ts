/// <reference path="./base-component.ts" />
namespace App {
      // ProjectList Class
      export class ProjectList extends Component<HTMLDivElement, HTMLElement> implements DragTarget {
        assignedProjects: Project[];
        constructor(private type: 'acitve' | 'finished') {
            super('project-list', 'app', false, `${type}-projects`);
            this.assignedProjects = []

            this.configure();
            this.renderContent();
        }
        @AutoBind
        dragOverHandler(event: DragEvent): void {
            if (event.dataTransfer && event.dataTransfer.types[0] === 'text/plain') {
                console.log('dragOverHandler');
                event.preventDefault();
                const listEl = this.element.querySelector('ul')!;
                listEl.classList.add('droppable');
            }

        }

        @AutoBind
        dropHandler(event: DragEvent): void {
            console.log('dropHandler>>>>>', event.dataTransfer!.getData('text/plain'));
            const prjId = event.dataTransfer!.getData('text/plain');
            projectState.moveProject(prjId, this.type === 'acitve' ? ProjectStatus.ACTIVE : ProjectStatus.FINISHED)
        }

        @AutoBind
        dragLeaveHandler(_event: DragEvent): void {
            console.log('dragLeaveHandler');

            const listEl = this.element.querySelector('ul')!;
            listEl.classList.remove('droppable');
        }
        private renderProjects() {
            const listEl = document.getElementById(`${this.type}-projects-list`)! as HTMLUListElement;
            listEl.innerHTML = '';
            for (let item of this.assignedProjects) {
                //             const listItem = document.createElement('li');
                //             listItem.textContent = item.title;
                // ;            listEl.appendChild(listItem);
                new ProjectItem(this.element.querySelector('ul')!.id, item);
            }
        }
        configure() {
            this.element.addEventListener('dragover', this.dragOverHandler);
            this.element.addEventListener('dragleave', this.dragLeaveHandler);
            this.element.addEventListener('drop', this.dropHandler);
            projectState.addListener((projects: Project[]) => {
                // filter
                const relevantProjects = projects.filter(prj => {
                    if (this.type === 'acitve') {
                        return prj.status === ProjectStatus.ACTIVE
                    }
                    return prj.status === ProjectStatus.FINISHED
                })
                this.assignedProjects = relevantProjects;

                this.renderProjects()
            })
        }
        renderContent() {
            let listId = `${this.type}-projects-list`;
            this.element.querySelector('ul')!.id = listId;
            this.element.querySelector('h2')!.textContent = this.type.toUpperCase() + ' PROJECTS';
        }

    }
}