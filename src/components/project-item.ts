namespace App {
     // ProjectItem Class
     export class ProjectItem extends Component<HTMLUListElement, HTMLLIElement> implements Draggable {
      private project: Project;
      constructor(hostId: string, project: Project) {
          super('single-project', hostId, false, project.id);
          this.project = project;
          this.configure();
          this.renderContent();
      }
      get persons() {
          if (this.project.people === 1) {
              return ' 1 person';
          } else {
              return `${this.project.people} persons`;
          }
      }
      configure(): void {
          this.element.addEventListener("dragstart", this.dragStartHandler);
          this.element.addEventListener('dragend', this.dragEndHandler);
      }
      renderContent(): void {
          this.element.querySelector('h2')!.textContent = this.project.title;
          this.element.querySelector('h3')!.textContent = this.persons;
          this.element.querySelector('p')!.textContent = this.project.description;
      }

      @AutoBind
      dragStartHandler(event: DragEvent): void {
          console.log('dragStartHandler');
          event.dataTransfer!.setData('text/plain', this.element.id);
          event.dataTransfer!.effectAllowed = 'move';

      }
      @AutoBind
      dragEndHandler(_event: DragEvent): void {
          console.log('dragEnd');
      }
  }
}