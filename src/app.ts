/// <reference path="./models/drag-drop.ts" />
/// <reference path="./models/project.ts" />
/// <reference path="./state/project-state.ts" />
/// <reference path="./util/validation.ts" />
/// <reference path="./decorators/autobind.ts" />
/// <reference path="./components/project-input.ts" />
/// <reference path="./components/project-list.ts" />


namespace App {
    console.log('>>> 爱学习！');
    new ProjectInput();
    new ProjectList('acitve');
    new ProjectList('finished');
}