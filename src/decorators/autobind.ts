namespace App {
      // autobind decorator
      export function AutoBind(_target: any, _methodName: string, descriptor: PropertyDescriptor) {
        console.log('descriptor >>>', descriptor);

        const originalMethod = descriptor.value;
        const adjDescriptor: PropertyDescriptor = {
            configurable: true,
            get() {
                const boundFn = originalMethod.bind(this);
                return boundFn;
            }
        }
        console.log('adjDescriptor >>>', adjDescriptor);

        return adjDescriptor;

    }
}